var kite_interval = 300
var kite_angle = 145
var scale = 150

setInterval(function(){
    var target = get_targeted_monster();    
    if (target != null){
        
        var target_position = [target.x, target.y]
        var player_position = [character.x, character.y]

        var vector = [target.x - character.x, target.y - character.y]
        var magnitude = distance(0,0,vector[0], vector[1]);

        var rotated_vector = rotate_vector(vector, degrees_to_radians(
            kite_angle
        ))

        var norm_rotated = [rotated_vector[0]/magnitude,
                              rotated_vector[1]/magnitude]
        
        
        move( target_position[0] + scale* norm_rotated[0], 
                    target_position[1] + scale * norm_rotated[1])
    }
},1000/4)